/* extension.js
 *
 */

/* exported init */

const GETTEXT_DOMAIN = 'extension-gnome-fredericpetitfr';

const { GObject, St, Gtk } = imports.gi;


const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Clutter   = imports.gi.Clutter


const _ = ExtensionUtils.gettext;

var Flag = GObject.registerClass(
    class FlagIndicator extends PanelMenu.Button {
        _init(text) {
            super._init(0.0, _('fredericpetit.fr'))

            this._label = new St.Label({ y_align: Clutter.ActorAlign.CENTER })
            this.add_actor(this._label);
            this.add_child(this._label);

            this.setText('❤');
            this.setVisible(true);

            this.addLink(_('Index Website fredericpetit.fr'), 'https://fredericpetit.fr/');
            this.addLink(_('Index Gitlab Frédéric Petit'), 'https://gitlab.com/fredericpetit/');
            this.addLink(_('Index Localhost:80'), 'https://localhost/');
            this.addLink(_('Index Localhost:8090 FredPHPMVC'), 'https://localhost:8090/');
            this.addLink(_('Index Localhost:8091 JSdifflib Legacy'), 'https://localhost:8091/');
            this.addLink(_('Index Localhost:8092 Adminer'), 'https://localhost:8092/');
            this.addLink(_('Index Localhost:8095 BookStack'), 'https://localhost:8095/');
        }

        addLink(label, url) {
            let item = new PopupMenu.PopupMenuItem(_(label));
            item.connect('activate', () => {
                this.menu.close();
                try {
                    Gtk.show_uri(null, url, global.get_current_time());
                }
                catch (err) {
                    let title = _("Can't open %s").format(url);
                    Main.notifyError(title, err);
                }
            });

            this.menu.addMenuItem(item);
        }

        setText(text) {
            this._label.set_text(text)
        }

        setVisible(visible) {
            this.container.visible = visible
        }
    }
)

class Extension {
    constructor(uuid) {
        this._uuid = uuid;
    }

    enable() {
        this._label = new Flag();
        Main.panel.addToStatusArea(this._uuid, this._label);
    }

    disable() {
        if (this._label) {
            this._label.destroy();
            this._label = null;
        }
    }
}

function init(meta) {
    ExtensionUtils.initTranslations(GETTEXT_DOMAIN);

    return new Extension(meta.uuid);
}